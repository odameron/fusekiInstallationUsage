---
title: Installation fuseki, reactome, ChEBI
tags: SPARQL, fuseki, BioPAX, Reactome, ChEBI
version: 1.1
date: 2023-04-14
lang: en
---

# 1. Installation

- install java
- download apache fuseki
    - from https://jena.apache.org/download/index.cgi
    - choose `apache-jena-fuseki-4.7.0.tar.gz` (or the zip file)
    - make sure that **fuseki** is in the file name
    - the tarball contains a directory, so go to the place where you want to extract this directory (e.g. `/usr/local`)
    - uncompress the tarball `tax xvfz apache-jena-fuseki-4.7.0.tar.gz && rm apache-jena-fuseki-4.7.0.tar.gz`
    - (optional) create a generic symbolic link to your fuseki directory: `ln -s apache-jena-fuseki-4.7.0 fuseki`
    - `cd fuseki`
    - edit the `fuseki-server` file to increase the heap size (for laaaaarge datasets)
        - replace the line `JVM_ARGS=${JVM_ARGS:--Xmx4G}` by `JVM_ARGS=${JVM_ARGS:--Xmx16G}`
    - you are (almost) all set. I find it convenient to declare a `${FUSEKI_HOME}` environment variable:
        - edit `~/.bashrc` and add a line `export FUSEKI_HOME=/usr/local/semanticWeb/fuseki` that points to wherever you installed fuseki
        - take your modification into account: `source ~/.bashrc`
- download some nice ontologies
    - tip: create an `ontology` directory somewhere :-)
    - BioPAX
        - ontology: http://www.biopax.org/release/biopax-level3.owl
        - documentation: http://www.biopax.org/release/biopax-level3-documentation.pdf
        - tutorial: https://gitlab.com/odameron/BioPAXtutorial
    - Reactome 
        - from https://reactome.org/download-data find (near the end of the page) "Events in the BioPAX level 3 format" that points to the stable URL https://reactome.org/download/current/biopax.zip
        - you will probably only need `Homo_sapiens.owl`
    - ChEBI
        - https://ftp.ebi.ac.uk/pub/databases/chebi/ontology/chebi.owl
- (optional) convenient python libraries
    - (mandatory) `python-sparqlwrapper`
		- installation: `pip3 install --user sparqlwrapper`
		- homepage https://github.com/RDFLib/sparqlwrapper
    - (advised) `sparqldataframe`
        - works on top of `python-sparqlwrapper` and converts the query result into a pandas dataframe
        - warning: do not confuse with `sparql-dataframe` which is not maintained and does not work very well
        - installation: `pip3 install --user sparqldataframe`


# 2. Use fuseki

## 2.1 Start the fuseki server (from a terminal)

- principle: `${FUSEKI_HOME}/fuseki-server --file=path/to/dataset1 [--file=path/to/dataset2] [--update] /datasetName`
    - use as many `--file=` arguments as necessary
    - using relative paths is OK, but I think I had troubles with `~` (probably because the arguments are processed directly by java)
    - use `--update` if you want to run queries that dynamically modify your dataset (e.g. `INSERT` or `DELETE` queries). If you don't know, you probably don't need it
    - choose whatever you like for `datasetName` (no space or weird characters)
- typical command (tip: put it in a script) `${FUSEKI_HOME}/fuseki-server --file=/home/olivier/ontology/reactome/Homo_sapiens-20230322.owl --file=/home/olivier/ontology/biopax/biopax-level3.owl --file=/home/olivier/ontology/chebi/chebi.owl /reactomeChEBI`
    - loading might take a while
    - when you see something like `Started 2023/04/13 08:30:04 CEST on port 3030` you server is ready
    - **do NOT close the terminal**


## 2.2 Perform queries

### 2.2.1 via the browser

- (for human users) at http://localhost:3030

### 2.2.2 via a program
- from programs at http://localhost:3030/datasetName/query (see below the two examples with `SPARQLWrapper` and `sparqldataframe`)


#### 2.2.2.1 Default `SPARQLWrapper` (you retrieve a dictionnary and do whatever you like with it)

```python
from SPARQLWrapper import SPARQLWrapper, JSON

# your sparql query
query = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT ?label
WHERE { <http://dbpedia.org/resource/Asturias> rdfs:label ?label }
"""

# declare a sparql endpoint
sparql = SPARQLWrapper("http://dbpedia.org/sparql")

# send your query and retrieve the result
sparql.setQuery(query)
sparql.setReturnFormat(JSON)
results = sparql.queryAndConvert()

# process the result
# note that "result" is a dictionary which keys
# are the variables in the SELECT clause
# of the SPARQL query (here "label")
for result in results["results"]["bindings"]:
	print(result["label"]["value"])
```


#### 2.2.2.2 With `sparqldataframe` (you retrieve a pandas DataFrame)

```python
import sparqldataframe

# your endpoint URL
endpointURL = "http://dbpedia.org/sparql"

# your sparql query
query = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT ?label
WHERE { <http://dbpedia.org/resource/Asturias> rdfs:label ?label }
"""

df = sparqldataframe.query(endpointURL, query)
```


# Useful links

- simple cheatsheet: https://gitlab.com/odameron/rdf-sparql-cheatsheet
- more detailed version: https://gitlab.com/odameron/rdf-sparql-tools
- SPARQL cheatsheet: https://github.com/relu91/sparqlCheatSheets
- Bob DuCharme
    - What is RDF? http://www.bobdc.com/blog/whatisrdf/
    - What is RDFS? http://www.bobdc.com/blog/whatisrdfs/ and https://www.bobdc.com/blog/whatisrdfspart2/
    - SPARQL in 11 minutes https://www.youtube.com/watch?v=FvGndkpa4K0
- W3C SPARQL1.1 specification https://www.w3.org/TR/sparql11-query/
